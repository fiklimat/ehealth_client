// store pro načtení časů

import {parse} from 'date-fns';
import moment from 'moment';
export default {

    // funkce načítající časy pro daného pacienta ze serveru
    async loadTimesForPatient(context,payload) {
        const response = await this.$axios.$get('/times/LoadTimesForPatient/' + payload);
        const line = response.split('\n');
        line.pop();
        const times = []
        for (const key in line) {
            const value = line[key]; // hodnota času ve formátu HL7 zpráv
            const disp = moment(parse(value, 'yyyyMMddHHmmss', new Date())).format('DD.MM.YYYY HH:mm:ss'); //hodnota času přeformátovaná pro zobrazení
            times.push({disp, value});
        }
        context.commit('setTimes', times);
        return times;
    }
};
