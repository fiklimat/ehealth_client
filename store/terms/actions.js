// store pro načtení terminologických termínů

export default {

    // funkce načítající termíny ze serveru
    async loadTerms(context) {
        const response = await this.$axios.$get('/terms/all');
        const line = response.split('\n');
        const terms = []
        for (const key in line) {
            const cols = line[key].split('|');
            const term = {Original: cols[0], SNOMED: cols[1], LOINC: cols[2]};
            terms.push(term);
        }
        context.commit('setTerms', terms);
        return terms;
    }
};
