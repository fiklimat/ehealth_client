// store pro načtení pacientských id

export default {

    //funkce načítající dostupná pacientská id ze serveru
    async loadPatientIds(context) {
        const response = await this.$axios.$get('/patient/allIds');
        const line = response.split('\n');
        line.pop();
        const patient_ids = []
        for (const key in line) {
            patient_ids.push(line[key]);
        }
        context.commit('setPatientIds', patient_ids);
        return patient_ids;
    }
};
