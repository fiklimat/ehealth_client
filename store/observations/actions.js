// store pro hlavní komunikaci se serverem pomocí HL7 nebo HL7 FHIR

import {parse} from 'date-fns';
export default {

    // funkce pro komunikaci pomocí HL7
    async loadObservationsHL7(context,payload) {

        //formátování ORM zprávy s vybranými parametry, časy a pacientským ID
        let datetime = new Date().toJSON().slice(0,19).replace(/-/g,'');
        datetime = datetime.replace('T', '');
        datetime = datetime.replace(/:/g,'');
        const ORM_message = "MSH|^~\\&|CLIENT APP|CLIENT APP|SERVER APP|SERVER APP|" + datetime + "||ORM^O01|" + datetime + "|P|2.4|||NE|AL|CZE|ASCII||ASCII\n" +
            "PID|||"+ payload.input.patient_id + "||^^^^^^L^A|||O\n" +
            "PV1||I|^^OR-1^10.2.56.5:1\n" +
            "ORC|RE\n" +
            "OBR|1|||" + payload.data_type + "|||" + payload.input.start_time.value + "|" + payload.input.end_time.value + "|||||||||||||||||G";
        console.log(ORM_message);

        // odeslání ORM a přijetí odpovědi serveru
        const response = await this.$axios.$post('/communication/getDataHL7',ORM_message);
        console.log(response);

        // přepsání odpovědi serveru do formátu požadovaného pro objekt grafu
        const line = response.split('\n');
        let message_id = "";
        let type_of_observation = "";
        const observations = [];
        let i = 0;
        for (const key in line) {
            if (key == 0) {
                const cols = line[key].split('|');
                message_id = cols[9];
            }
            if (key>4 && line[key] !== '') {
                const cols = line[key].split('|');
                if (type_of_observation === "") {
                    type_of_observation = cols[3];
                    observations[i] = {name: "", data: []};
                    observations[i].name = cols[3];
                } else if (type_of_observation !== cols[3]) {
                    i = i + 1;
                    type_of_observation = cols[3];
                    observations[i] = {name: "", data: []};
                    observations[i].name = cols[3];
                }
                const value = parseInt(cols[5]);
                const date_time = parse(cols[14], 'yyyyMMddHHmmss', new Date());
                observations[i].data.push([date_time, value]);
            }
        }

        // uložení a vrácení přeformátovaných dat ze serveru
        context.commit('setObservations', observations);
        return [observations, message_id];
    },

    // odeslání MSA
    async sendMSA(context, payload) {
        const MSA_message = "MSH|^~\\&|||||||ACK^R01^ACK|" + payload.id + "|P|||||||ASCII||ASCII\n" +
            "MSA|AA|" + payload.id;
        console.log(MSA_message);
        await this.$axios.$post('/mSA/sendMSA',MSA_message);
    },

    // funkce pro komunikaci pomocí HL7 FHIR
    async loadObservationsHL7_FHIR(context,payload) {
        let datetime = new Date().toJSON().slice(0, 19).replace(/-/g, '');
        datetime = datetime.replace('T', '');
        datetime = datetime.replace(/:/g, '');

        //formátování Diagnostic Report zprávy s vybranými parametry, časy a pacientským ID
        let message = {
            resourceType : "DiagnosticReport",
            identifier : {
                value : datetime
            },
            status : "final",
            code : {
                coding : payload.data_types
            },
            subject : {
                resourceType : "Patient",
                identifier : [{
                    value : payload.input.patient_id
                }],
            },
            effectivePeriod : {
                start : payload.input.start_time.value,
                end : payload.input.end_time.value
            }
        }
        console.log(message);

        // odeslání Diagnostic Report a přijetí odpovědi serveru
        const response = await this.$axios.$post('/communication/getDataHL7FHIR',message);
        console.log(response);

        // přepsání odpovědi serveru do formátu požadovaného pro objekt grafu
        const observations = [];
        let type_of_observation = "";
        let i = 0;
        for (const key in response) {
            const current_type_of_observation = response[key].code.coding[0].code + '^' + response[key].code.coding[0].display + '^' + response[key].code.coding[0].system;
            if (type_of_observation === "") {
                type_of_observation = current_type_of_observation;
                observations[i] = {name: "", data: []};
                observations[i].name = current_type_of_observation;
            } else if (type_of_observation !== current_type_of_observation) {
                i = i + 1;
                type_of_observation = current_type_of_observation;
                observations[i] = {name: "", data: []};
                observations[i].name = current_type_of_observation;
            }
            const value = parseInt(response[key].valueQuantity.value);
            const date_time = parse(response[key].effectiveDateTime, 'yyyyMMddHHmmss', new Date());
            observations[i].data.push([date_time, value]);
        }
        return observations;
    }
};
